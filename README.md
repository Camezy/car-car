# CarCar
CarCar is an application that helps you manage all aspects of an automobile dealership. It allows you to keep track of your inventory, handle automobile sales, and manage automobile services.
Team:

* Vivian Martinez - Service
* Cameron Ross - Sales microservice


## Getting Started
When you fork this repository, you will need to have Docker , Git and Node.js 18.2. To clone the forked repostiry onto your local machine use the following command.
git clone <<respository.url.here>>

<docker volume create beta data> will build your volume, because of this you will not need to create a virtual enviorment.

<docker-compose build> will create your images.

<docker-compose up> will create your containers.
​
View the project in the browser: http://localhost:3000/

## Design

- **Inventory**
- **Services**
- **Sales**
![Alt text](microservice_two-shot.png)

## Service microservice
An automobile must be created before being able to make an appointment. The appointment uses the vin to note VIP or not if it was sold from our inventory. The following steps will give you a manufacturer,

We used Insomnia to create a automile in Incentory:
Create a manufacturer
POST http://localhost:8100/api/manufacturers/
JSON body:
{
  "name": "Chrysler"
}

Create a vehicle model
POST http://localhost:8100/api/models/
JSON body:
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
  "manufacturer_id": 1
}

Create an automobile
"POST" http://localhost:8100/api/manufacturers/
JSON body:

{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}

To see a list of autombiles in Insomnia:
"GET"  http://localhost:8100/api/automobiles/
Expected output if data is present:
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "red",
      "year": 2012,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "image.yourpictureurl.com",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      }
    }
  ]
}
------------------------------
Our Inventory and Sales domains work together with our Service domain to make everything here at **CarCar** possible. Using a **'poller'**  which talks to the inventory domain to keep track of which vehicles we have in our inventory so that the service and sales team always has up-to-date information.

Service Insomnia instruction. Before scheduling or seeing an appointment, Technicians have to be created. When the technician is created, the tech can also be deleted. After doing so, we can now make appointments. The **'vin'** at the end of the urls represents the VIN for the specific automobile you want to access. This is not an integer ID. This is a string value so you can use numbers and/or letters. If the VIN is with the automobile that was sold from the inventory, the appointment will be marked VIP.

| List technicians | GET | http://localhost:8080/api/technicians/
Body: N/a
Expected return if data present:
{
	"tech": [
		{
			"first_name": "Junior",
			"last_name": " Martinez",
			"employee_id": "jrmtz",
			"id": 1
		}
	]
}
------------------------------
| Create a technician | POST | http://localhost:8080/api/technicians/
Body: JSON
{
	"first_name": "Eddie",
	"last_name": "Martinez",
	"employee_id": "Emartie"
}
Expected return if data present:
{
	"first_name": "Eddie",
	"last_name": "Martinez",
	"employee_id": "Emartie",
	"id": 3
}
------------------------------
| Delete a specific technician | DELETE | http://localhost:8080/api/technicians/:id
Body: N/a
Expected return if data present:
{
	"first_name": "Eddie",
	"last_name": "Martinez",
	"employee_id": "Emartie",
	"id": null
}
------------------------------
After technicians are made. Appointments can now be created. Remember that the vin in some of the URls are string formated and is the VIN from the automobile created in Inventory. The appopintment will show a status of created. If the car was shown sold in our inventory, it will give show "Yes" for VIP. Once the appointment is made, it will show in the list. From there you have the option to cancel or finish the appointment. You can look at the appointment history by searching the VIN in the search bar.

| List appointments | GET | http://localhost:8080/api/appointments/
Body: N/a
Expected return if data present:
{
	"appointments": [
		{
			"reason": "oil change",
			"vin": "ABCDEF",
			"customer": "test customer",
			"technician": {
				"first_name": "Junior",
				"last_name": " Martinez",
				"employee_id": "jrmtz",
				"id": 1
			},
			"date": "2023-12-21",
			"time": "16:20:41.743221",
			"id": 1,
			"status": "created"
		},
	]
}
------------------------------
| Create an appointment | PUT | http://localhost:8080/api/appointments/
Body: JSON
{
	"reason": "windshield",
	"vin": "1C3CC5FB2AN120174",
	"customer": "Sierra Stock",
	"technician": 3,
	"date": "12/20/2023",
	"time": "12:00"
}
Expected return if data present:
{
	"reason": "windshield",
	"vin": "1C3CC5FB2AN120174",
	"customer": "Sierra Stock",
	"technician": {
		"first_name": "Eddie",
		"last_name": "Martinez",
		"employee_id": "Emartie",
		"id": 3
	},
	"date_time": "2023-12-21T00:00:41.475985+00:00",
	"id": 6,
	"status": "created"
}
------------------------------
| Delete an appointment by vin | DELETE | 	http://localhost:8080/api/appointments/vin/
Body: N/a
Expected return if data present:
{
	"message": "Appointment deleted"
}
------------------------------
| Set appointment status to "canceled" | PUT | http://localhost:8080/api/appointments/vin/cancel/
Body: JSON
{
	"status": "cancelled"
}
Expected return if data present:
{
	"status": "cancelled",
	"date_time": "2023-12-20T00:55:58.237520+00:00",
	"reason": "oil change",
	"vin": "1C3CC5FB2AN120174",
	"customer": "Sierra Stock",
	"technician": "Vivian Martinez"
}
------------------------------
| Set appointment status to "finished" | PUT | http://localhost:8080/api/appointments/vin/finish/
Body: JSON
{
	"status": "finished"
}
Expected return if data present:
{
	"status": "finished",
	"date_time": "2023-12-20T00:55:58.237520+00:00",
	"reason": "oil change",
	"vin": "1C3CC5FB2AN120174",
	"customer": "Sierra Stock",
	"technician": "Vivian Martinez"
}
------------------------------
The value object used in for inventory was Automobile value object. This was to poll the information from inventory automobiles. We used the properties vin and sold.

requests.get("http://project-beta-inventory-api-1:8000/api/automobiles/")


## Sales microservice

In Our Sales Microservice we’ve incorporated some models that are interconnected with one another as well as implementing a VO, Value Object, to use as a cross reference or bridge if you will to the other service, the main service, to use information that is needed in our own microservice. So, for the backend we created models that we want to use to display data in our friend and 3 party application in insomnia in addition to the Port Number that our micro service will be using. Service Microserivce will be using Port 8080, Sales Microservice 8090, Inventory(the main) 8100, all interconnected and running on Port 3000 as the server for the application. So each service has it own car, to get to CARCAR’s house, which is Port 3000.

Make sure that you add your app to your settings and make sure you also add the corsheaders (that will be an issue later on, so make sure you take care of it early now)

So in sales we’ve created 4 models which includes one VO model as a stand-in representation of the original model that is used in another service. Think of it as a stunt double. From there upon creating these models, we need to make some view functions and logic in order to get our data functioning properly. Because the Client calls for only displaying key information with out much of the extra, we did our best to implent the criteria that was expressed.

For our Models, with a proper request we formulated links to place in our insomnia to check for connection (200 OK) and a cross reference to use later in our frontend:

Based on Customers Model
GET: http://localhost:8090/api/customers/
POST: http://localhost:8090/api/customers/
DELETE: http://localhost:8090/api/customers/:id (Link wasn’t used due to the nature of the project but was made on stand by for functionality purposes)



Based on Salesperson Model
GET:http://localhost:8090/api/salespeople/
POST: http://localhost:8090/api/salespeople/
DELETE: http://localhost:8090/api/customers/:id (Was not used)


Based on Sales  Model
GET: http://localhost:8090/api/sales/
POST: http://localhost:8090/api/sales/
DELETE: http://localhost:8090/api/sales/id (Was not used)


Also for the AutomobileVO Model:
GET: http://localhost:8100/api/automobiles/
POST: http://localhost:8100/api/automobiles/
GET: http://localhost:8100/api/automobiles/:vin/
PUT: http://localhost:8100/api/automobiles/:vin/
DELETE: http://localhost:8100/api/automobiles/:vin/

Also, in Insomnia we also included the other services Model;
Vehicle Model, Manufactures and Automobiles. They all follow the same layout with the url associated with them.

It should have something along the lines of this:
{
	"autos": [
		{
			"href": "/api/automobiles/ASDFGHJKL/",
			"id": 1,
			"color": "Red",
			"year": 2024,
			"vin": "ASDFGHJKL",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Tesla Model S",
				"picture_url": "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.edmunds.com%2Ftesla%2Fmodel-s%2F&psig=AOvVaw3mi9UvYFCVkbTmnd8q6bJ5&ust=1703101554487000&source=images&cd=vfe&opi=89978449&ved=0CBIQjRxqFwoTCKjzi-y",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Tesla"
				}
			},
			"sold": false
		}
	]
}
Grante this will vary from model to model, but this is a general basis of what to expect from Insomnia


Aside from having that set up, we created some view functions to help display our information needed when creating a form and displaying a list of members or objects of that list. What was done was that a separate file was created for Encoders.py to house only our encoders and keep our views file clean and readable.

For the main models, models that do not have a VO attached, we implemented both a Listing and Detailing Encoder for them so that when asking for detailed information we can access that information and apply some of it when we list out data. Having that capability to separate the different forms of information that we have.

In the AutomobileVODetail Encoder, in that model there all was used was the two properties that was use back when the model was created. So for this Model Encoder Vin and Sold was used.


Now, assuming that you have made all the proper imports to all the files in your micro service, you need to give them a url to access. So import your views into your url and then from that point we listed out each view name that will be called to show a list(GET), post(create) and a delete if necessary when referring to an ID/VIN. For each function that will list off a bunch of data, in the url you will leadoff with a forward slash. Like so:
path("salespeople/", api_list_salesperson, name="api_list_salesperson"),

and for a more specified object/item you will lead off with just that unique identifier:
path("salespeople/<int:id>", api_show_salesperson, name="api_show_salesperson"),

This will be done for each view function created to access that data.


Next a poller had to be created and this is where the VO had to shine since it is the only outside source data we need from a different microservice.

In that we created a function that created a json response in an object that was able to retrieve that information from port 8000. Also to mention that during this process you will need to create some data in the admin panel to ensure that you are retrieving the proper data for your said microservice.

From there you will want to use that function name that you used to create that logic and add it to the def poll so that it can run during its while loop.


Moving on over to our Front-end, this is where we start to tie everything up and display the good information. In ghi/src file, you will create a folder, name components that will house all the data as far as the create and list data for our clients to fill out and view. Next in those files you will create a create and list file for each model that was created that (Except any that was a VO because you would be making that twice) that will route through the Apps.js file as well as the Nav.js file.


Assuming that all the information was handled correctly you can use insomnia to check for connectivity and debug handling incase of any issues connecting the shared information together. After hit CMD+OPT+J and checking for any console log errors all sights should be operating successfully

Also to check for some of your insomnia list here are a couple of more examples to follow along with:
For Manufacturers
__________________________________
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Tesla"
		},]

For General Sales
__________________________________
{
	"sales": [
		{
			"price": "54000.00",
			"automobile": "ASDFGHJKL",
			"salesperson": {
				"employee_id": "12345678",
				"first_name": "Jake",
				"last_name": "StateFarm"
			},
			"customer": {
				"first_name": "Jen",
				"last_name": "Hen"
			}
		}
	]
}

For Customers
__________________________________
	"customers": [
		{
			"first_name": "Cameron",
			"last_name": "R",
			"address": "721 Arboretum Way",
			"phone_number": 2112212121
		}]
