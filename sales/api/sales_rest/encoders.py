from common.json import ModelEncoder
from .models import Salesperson, Customer, Sale, AutomobileVO


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold"]


class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = ["price", "customer", "salesperson", "automobile"]
    encoders = {
        "automobile": AutomobileVO(),
        "salesperson": Salesperson(),
        "customer": Customer(),
    }


class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = ["price"]

    def get_extra_data(self, o):
        return {
            "automobile": o.automobile.vin,
            "salesperson": {
                "employee_id": o.salesperson.employee_id,
                "first_name": o.salesperson.first_name,
                "last_name": o.salesperson.last_name,
            },
            "customer": {
                "first_name": o.customer.first_name,
                "last_name": o.customer.last_name,
            },
        }


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "address", "phone_number"]
    encoders = {
        "automobile": AutomobileVO(),
    }


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "address", "phone_number"]


class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_id"]


class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_id"]
    encoders = {
        "automobile": AutomobileVO(),
    }
