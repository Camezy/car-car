from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import (
    TechnicianEncoder,
    AppointmentEncoder,
)
from .models import AutomobileVO, Technician, Appointment


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"tech": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Could not create technician"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE"])
def api_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.object.get(id=pk)
            return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        request.method == "DELETE"
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_list_appointment(request, technician_name=None):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician_name = content["technician"]
            technician = Technician.objects.get(pk=technician_name)
            content["technician"] = technician
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Could not make an appointment"},
                status=400,
            )


@require_http_methods(["GET"])
def api_inventory(request, vin):
    try:
        automobile = AutomobileVO.objects.get(vin=vin)
        is_in_inventory = True if automobile.sold is True else False
        return JsonResponse({"isInInventory": is_in_inventory})
    except AutomobileVO.DoesNotExist:
        return JsonResponse(
            {"isInInventory": False},
            status=400,
        )


@require_http_methods(["DELETE"])
def api_appointment(request, vin):
    if request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(vin=vin)
            appointment.delete()
            return JsonResponse(
                {"message": "Appointment deleted"},
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["PUT"])
def api_finish_appointment(request, vin):
    try:
        appointment = Appointment.objects.get(vin=vin)
        appointment.finish()
        body = {
            "status": appointment.status,
            "vin": appointment.vin,
            "customer": appointment.customer,
        }
        return JsonResponse(
            body,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Appointment not found"}, status=404)


@require_http_methods(["PUT"])
def api_cancel_appointment(request, vin):
    try:
        appointment = Appointment.objects.get(vin=vin)
        appointment.cancel()
        body = {
            "status": appointment.status,
            "vin": appointment.vin,
            "customer": appointment.customer,
        }
        return JsonResponse(
            body,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Appointment not found"}, status=404)
