import React, { useEffect, useState } from "react";

function SalesPeopleForm() {
  const [automobiles, setAutomobiles] = useState([]);

  const [formData, setFormData] = useState({
    first_name: "",
    last_name: "",
    employee_id: "",
  });

  const fetchData = async () => {
    const url = "http://localhost:8100/api/automobiles/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.automobiles);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const url = "http://localhost:8090/api/salespeople/";

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        first_name: "",
        last_name: "",
        employee_id: "",
      });
    }
  };

  const handleFormChange = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Salesperson</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="first_name"
                required
                type="text"
                name="first_name"
                id="first_name"
                className="form-control"
                value={formData.first_name}
              />
              <label htmlFor="name">First name...</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="last_name"
                required
                type="text"
                name="last_name"
                id="last_name"
                className="form-control"
                value={formData.last_name}
              />
              <label htmlFor="picture">Last name...</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="employee_id"
                required
                type="text"
                name="employee_id"
                id="employee_id"
                className="form-control"
                value={formData.employee_id}
              />
              <label htmlFor="employee_id">Employee ID...</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SalesPeopleForm;
