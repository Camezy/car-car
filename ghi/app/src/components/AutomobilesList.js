import { useEffect, useState } from "react";

function AutomobilesList() {
  const [autos, setAutos] = useState([]);
  const [models, setModels] = useState([]);
  const getData = async () => {
    const request = await fetch("http://localhost:8100/api/automobiles/");
    if (request.ok) {
      const response = await request.json();
      setAutos(response.autos);
    }
    const response = await fetch("http://localhost:8100/api/models/");
    if (response.ok) {
      const request = await response.json();
      setModels(request.models);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
      <h1>Automobiles</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {autos.map((auto) => {
            return (
              <tr key={auto.id}>
                <td>{auto.vin}</td>
                <td>{auto.name}</td>
                <td>{auto.name}</td>
                <td>{auto.model.name}</td>
                <td>{auto.model.manufacturer.name}</td>
                <td>{auto.sold}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default AutomobilesList;
