import React, { useEffect, useState } from "react";

function AutomobilesForm() {
  const [automobiles, setAutomobiles] = useState([]);
  const [models, setModels] = useState([]);
  const [formData, setFormData] = useState({
    color: "",
    year: "",
    vin: "",
    sold: "",
    model: "",
  });

  const fetchData = async () => {
    const url = "http://localhost:8100/api/automobiles/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.automobiles);
    }

    const modelsUrl = "http://localhost:8100/api/models/";
    const modelsResponse = await fetch(modelsUrl);
    if (modelsResponse.ok) {
      const data = await modelsResponse.json();
      setModels(data.models);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const url = "http://localhost:8100/api/automobiles/";

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        color: "",
        year: "",
        vin: "",
        sold: "",
        model: "",
      });
    }
  };

  const handleFormChange = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add an automobile to inventory</h1>
          <form onSubmit={handleSubmit} id="create-automobile-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
                value={formData.color}
              />
              <label htmlFor="">Color...</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="year"
                required
                type="text"
                name="year"
                id="year"
                className="form-control"
                value={formData.year}
              />
              <label htmlFor="">Year...</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="vin"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
                value={formData.vin}
              />
              <label htmlFor="">VIN...</label>
            </div>

            <div className="mb-3">
              <select
                value={formData.model}
                onChange={handleFormChange}
                required
                name="model"
                id="model"
                className="form-select"
              >
                <option value="">Choose a model...</option>
                {models.map((model) => {
                  return (
                    <option key={model.id} value={model.name}>
                      {model.name}
                    </option>
                  );
                })}
              </select>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default AutomobilesForm;
