import { useEffect, useState } from "react";

function SalesPeopleList() {
  const [salespersons, setSalespersons] = useState([]);

  const getData = async () => {
    const request = await fetch("http://localhost:8090/api/salespeople");
    if (request.ok) {
      const response = await request.json();
      setSalespersons(response.salespersons);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
      <h1>Salespeople</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {salespersons
            .sort((a, b) => a.id - b.id)
            .map((salesperson) => {
              return (
                <tr key={salesperson.employee_id}>
                  <td>{salesperson.employee_id}</td>
                  <td>{salesperson.first_name}</td>
                  <td>{salesperson.last_name}</td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );
}

export default SalesPeopleList;
