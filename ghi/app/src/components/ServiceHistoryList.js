import React, { useEffect, useState } from "react";

function ServiceHistoryList() {
  const [appointments, setAppointment] = useState([]);
  const [autos, setInventory] = useState([]);
  const [filterValue, setFilterValue] = useState("");

  const getData = async () => {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      setAppointment(data.appointments);
    }
  };
  const getInventory = async () => {
    const response = await fetch("http://localhost:8100/api/automobiles/");
    if (response.ok) {
      const data = await response.json();
      setInventory(data.autos);
    }
  };
  useEffect(() => {
    getData();
    getInventory();
  }, []);

    function handleFilterChange(event) {
        setFilterValue(event.target.value)
    }
    return (
        <div>
            <div className="row">
                <div className="col">
                    <h1>Service History</h1>
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <input onChange={handleFilterChange} className="form-control" placeholder="Search by Vin..." />
                </div>
                <div className="col-auto">
                <button className="btn btn-sm btn-outline-secondary" type="button">Search</button>
                </div>
            </div>

            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments
                        .filter((appointments) => appointments.vin.includes(filterValue))
                        .map(appointments => {
                            return (
                                <tr key={appointments.id}>
                                    <td>{appointments.vin}</td>
                                    <td>{autos.find(auto => auto.vin === appointments.vin) ? 'Yes' : 'No'}</td>
                                    <td>{appointments.customer}</td>
                                    <td>{appointments.date}</td>
                                    <td>{appointments.time}</td>
                                    <td>{appointments.technician.first_name} {appointments.technician.last_name}</td>
                                    <td>{appointments.reason}</td>
                                    <td>{appointments.status}</td>
                                </tr>
                            );
                        })}
                </tbody>
            </table>
        </div>
    )
}
export default ServiceHistoryList;
